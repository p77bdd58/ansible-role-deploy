deploy
======

Deploy from git repo to document root, nginx server block for http{s} node apps

**HTTPS**  
- Redirect HTTP to HTTPS
- [HTTP Strict Transport Security support](http://stackoverflow.com/a/4365482)


Requirements
------------

- nginx installed as web sever.


Role Variables
--------------

```
app: node
app_name: hello_world
app_repo: https://bencooling@bitbucket.org/bencooling/node-hello-world.git
app_port: 3000
app_server_name: server
app_tls: false
app_tls_cert: '{{playbook_dir}}/ssl/{{app_name}}.pem'
app_tls_key: '{{playbook_dir}}/ssl/{{app_name}}.key'
```


Dependencies
------------

No dependencies.


Example Playbook
----------------

```
---
- hosts: server
  sudo: true
  roles:
    - { role: deploy,
        app_tls: true,
        app_tls_cert: '{{playbook_dir}}/ssl/hello_world.pem',
        app_tls_key: '{{playbook_dir}}/ssl/hello_world.key'
      }
```

License
-------

BSD

Author Information
------------------

[bcooling.com.au](bcooling.com.au)
